from kivy.app import App
from kivy.uix.widget import Widget
from kivy.graphics import Color, Ellipse
from kivy.core.window import Window
from kivy.clock import Clock
from datetime import datetime
from marker import AndroidBluetoothClass

BLACK = (0, 0, 0)
RED = (1, 0, 0)
GREEN = (0, 1, 0)
WHITE = (1, 1, 1)
RADIUS = 25

AndroidBluetooth = AndroidBluetoothClass()
try:
    AndroidBluetooth.getAndroidBluetoothSocket(device_name="ESP_SPP_ACCEPTOR")
except:
    pass


class TimeLimitException(Exception):
    pass


class Target(Widget):

    def __init__(self, **kwargs):
        super(Target, self).__init__(**kwargs)
        self.central_target = (Window.width // 2 - RADIUS, Window.height // 2 - RADIUS)
        self.show_central_target = True
        self.target_count = 0  # Counter for target touch events
        self.update_time = 1.0
        self.max_target_count = 100
        Clock.schedule_interval(self.update, self.update_time)

    def update(self, dt):
        self.canvas.clear()
        with self.canvas:
            if self.show_central_target:
                Color(*WHITE)
                Ellipse(pos=self.central_target, size=(RADIUS * 2, RADIUS * 2))
                self.send_eeg_marker(1)
                self.show_central_target = False
                self.target_count += 1
            else:
                self.show_central_target = True
                self.canvas.clear()
                self.send_eeg_marker(0)
        # if (self.update_time <= 1/60):
        #     raise(TimeLimitException)
        if self.target_count == self.max_target_count:
            self.target_count = 0
            self.max_target_count = self.max_target_count * 2
            self.update_time = self.update_time / 2
            Clock.unschedule(self.update)
            Clock.schedule_interval(self.update, self.update_time)

    def send_eeg_marker(self, message):
        i = [message]
        pre = bytearray(i)
        # cmd = 'Target Pressed\n'.encode('UTF-8')
        # #extend bytearray
        # pre.extend(cmd)
        if AndroidBluetooth.ConnectionEstablished:
            AndroidBluetooth.BluetoothSend(pre)
        else:
            print("No BT Connection")

class TimingTestApp(App):

    def build(self):
        return Target()


if __name__ == "__main__":
    try:
        TimingTestApp().run()
    except TimeLimitException:
        pass
