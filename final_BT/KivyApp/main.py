from ast import Return
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivy.graphics import Color, Ellipse
from kivy.core.window import Window
from kivy.clock import Clock
from datetime import datetime
from marker import AndroidBluetoothClass
from threading import Thread
import time


BLACK = (0, 0, 0)
RED = (1, 0, 0)
GREEN = (0, 1, 0)
WHITE = (1, 1, 1)
RADIUS = 25
MAKE_CSV = False

class Target(Widget):

    def __init__(self, AndroidBluetooth, **kwargs):
        super(Target, self).__init__(**kwargs)
        self.AndroidBluetooth = AndroidBluetooth
        self.central_target = (Window.width // 2 - RADIUS, Window.height // 2 - RADIUS)
        self.show_central_target = True
        self.target_count = 0  # Counter for target touch events
        self.message_count = 0
        self.update_time = 0.5
        self.sent_time = 0
        self.rec_time = 0
        self.start_time = self.get_time()
        #self.max_target_count = 100
        #Clock.schedule_interval(self.update, self.update_time)

    def update(self, dt):
        self.canvas.clear()            
        with self.canvas:
            if self.show_central_target:
                Color(*WHITE)
                Ellipse(pos=self.central_target, size=(RADIUS * 2, RADIUS * 2))
                self.send_marker(str(self.get_time() - self.start_time)) #target
                self.show_central_target = False
                self.target_count += 1
                self.message_count += 1
            else:
                self.show_central_target = True
                self.canvas.clear() #no target
                self.send_marker(str(self.get_time() - self.start_time)) #no target
                self.message_count += 1

    def time_update(self, dt):
        self.update_time = dt
        Clock.unschedule(self.update)
        Clock.schedule_interval(self.update, self.update_time)

    def get_time(self):
        return round(time.time() * 1000)
        
    def send_marker(self, message):
        cmd = bytearray(message.encode('UTF-8'))
        if self.AndroidBluetooth.ConnectionEstablished:
            self.sent_time = self.get_time()
            self.AndroidBluetooth.BluetoothSend(cmd)
        else:
            print("No BT Connection")
    
    def receive_marker(self):
        if self.AndroidBluetooth.ConnectionEstablished:
            message = self.AndroidBluetooth.BluetoothReceive()
            return message
        else:
            print("No BT Connection")
            return None

    def bluetooth_listener(self):
        while True:
            message = self.receive_marker() #will wait till gets a message
            self.rec_time = self.get_time()
            latency = self.rec_time - self.sent_time
            if MAKE_CSV:
                print(self.sent_time, self.rec_time, latency, message, sep=", ")

    def init_csv(self):
        headers = "sent_time, rec_time, latency, message"
        print(headers)  
    

class TimingTestApp(App):

    def build(self):
        self.AndroidBluetooth = AndroidBluetoothClass()

        self.layout = BoxLayout(orientation='vertical')
        self.target = Target(self.AndroidBluetooth)

        # Create buttons and add them to the layout
        button1 = Button(text='1000ms', on_press=lambda x: self.target.time_update(1.0))
        button2 = Button(text='500ms', on_press=lambda x: self.target.time_update(0.5))
        button3 = Button(text='250ms', on_press=lambda x: self.target.time_update(0.25))
        button4 = Button(text='100ms', on_press=lambda x: self.target.time_update(0.1))
        button5 = Button(text='50ms', on_press=lambda x: self.target.time_update(0.05))

        button_layout = BoxLayout(size_hint=(1, None), height=50)
        button_layout.add_widget(button1)
        button_layout.add_widget(button2)
        button_layout.add_widget(button3)
        button_layout.add_widget(button4)
        button_layout.add_widget(button5)

        self.layout.add_widget(button_layout)

        # Create popup for user to input update time
        self.textinput = TextInput(text='500', multiline=False)
        self.popup = Popup(title='Enter initial update time in ms',
                           content=self.textinput,
                           size_hint=(None, None), size=(400, 200),
                           auto_dismiss=False)
        self.textinput.bind(on_text_validate=self.on_answer)
        self.popup.open()

        return self.layout

    def on_start(self):
        try:
            self.AndroidBluetooth.getAndroidBluetoothSocket(device_name="ESP_SPP_ACCEPTOR")
        except:
            pass
        self.layout.add_widget(self.target)

    def on_answer(self, instance):
        self.target.time_update(float(instance.text) / 1000)
        if MAKE_CSV:
            listener_thread = Thread(target=self.target.bluetooth_listener)
            listener_thread.start()
            self.target.init_csv()
        self.popup.dismiss()
        #self.layout.add_widget(self.target)

if __name__ == "__main__":
    TimingTestApp().run()
    