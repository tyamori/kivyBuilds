# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

cmake_minimum_required(VERSION 3.5)

file(MAKE_DIRECTORY
  "/Users/tyleryamori-little/esp/esp-idf/components/bootloader/subproject"
  "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader"
  "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix"
  "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix/tmp"
  "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix/src/bootloader-stamp"
  "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix/src"
  "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix/src/bootloader-stamp"
)

set(configSubDirs )
foreach(subDir IN LISTS configSubDirs)
    file(MAKE_DIRECTORY "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix/src/bootloader-stamp/${subDir}")
endforeach()
if(cfgdir)
  file(MAKE_DIRECTORY "/Users/tyleryamori-little/kivyBuild/final_BT/BT_SPP/build/bootloader-prefix/src/bootloader-stamp${cfgdir}") # cfgdir has leading slash
endif()
